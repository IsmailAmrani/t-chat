<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/message.css">
    
    <title>T-Chat -- Messenger</title>
  </head>
  <body>
      
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal">T-CHAT</h5>
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark" href="#">BONJOUR <b><?php echo $user['nom'] . " " .$user['prenom'] ?></b></a>
  </nav>
  <a class="btn btn-outline-primary" href="logout.php">Se Déconnecter</a>
</div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h4 class="display-4">MES MESSAGES</h4>
    </div>

    <div class="container">
        
        <div class="row">
            <div class="col-md-4 row">
                <div class="col-md-12 mb-4">
                    <h5>EN LIGNE</h5>
                    <?php foreach ($con_users as $key => $con_user){ ?>
                    <div id="user_c<?php echo $key ?>" class="conected" onclick="SelectConversation('<?php echo $con_user['email'] ?>', 'user_c<?php echo $key ?>')"><?php echo $con_user['nom'] . " " .$con_user['prenom'] ?></div>
                    <?php } ?>
                </div>
                <div class="col-md-12">
                    <h5>HORS LIGNE</h5>
                    <?php foreach ($discon_users as $key => $discon_user){ ?>
                    <div id="user_d<?php echo $key ?>" class="disconected" onclick="SelectConversation('<?php echo $discon_user['email'] ?>', 'user_d<?php echo $key ?>')"> <?php echo $discon_user['nom'] . " " .$discon_user['prenom'] ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-8">
                <div id="ListMessages" class="Messages">
                </div>
                <div class="row mt-3">
                    <div class="col-md-8">
                        <div class="form-group">
                        <input type="email" class="form-control" id="messageText" aria-describedby="emailHelp" placeholder="Message">
                        <small id="emailHelp" class="form-text text-muted">Votre Message Ici.</small>
                      </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <button id="btn_send" disabled onclick="SendMessage()" type="button" class="btn btn-info">Envoyer</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../web/javascript/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="javascript/script.js"></script>
  </body>
</html>