<?php

namespace src\entity;

/**
 * User
 */
class User
{
    private $nom;

    private $prenom;

    private $gender;

    private $email;

    private $password;

    private $is_connected;
    
    private $id;


    /**
     * Set nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set email
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get id
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get password
     */
    public function getIsConnected()
    {
        return $this->is_connected;
    }

    /**
     * Get id
     */
    public function setIsConnected($is_connected)
    {
        $this->is_connected = $is_connected;

        return $this;
    }
}

