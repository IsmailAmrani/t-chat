<?php

namespace src\controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author eaismail
 */
class MessageController {
    
    /**
     * Message Page
     */
    public function Message(){
        
        $CurrentuserEmail = $_SESSION['user'];
        
        $userManager = new \src\manager\UserManager;
        
        //Get users Informations
        $user = $userManager->getUser($CurrentuserEmail);
        
        //Get Connected users
        $ConnectUsers = $userManager->GetUsersConnected($CurrentuserEmail);
        //Get Disonnected users
        $DisconnectUsers = $userManager->GetUsersDisconnected($CurrentuserEmail);
        
        echo $this->render(array(
            'user' => $user,
            'con_users' => $ConnectUsers,
            'discon_users' => $DisconnectUsers,
                ), '../app/views/messenger.php');
        
    }
    
    /**
     * Create conversation in not existe
     */
    public function CreateConversation(){
        $messageManager = new \src\manager\MessageManager();
        $messageManager->CreateConversation($_SESSION['user'], $_POST['email']);
        $_SESSION['conversation'] = $_POST['email'];
    }
    
    /**
     * Get messages by conversation
     */
    public function getMessagesByCurrentConversation(){
        
        $messageManager = new \src\manager\MessageManager();
        $userManager = new \src\manager\UserManager();
        
        $current_Conversation = $messageManager->CreateConversation($_SESSION['user'], $_SESSION['conversation']);
        $messages = $messageManager->getMessageByConversation($current_Conversation);
        
        $All_Messages = array();
        foreach ($messages as $message){
            
            $item = array();
            
            $item['date'] = $message['date'];
            $sender = $userManager->getUserById($message['sender']);
            
            if($sender['email'] == $_SESSION['user']){
                $item['type'] = "sender";
            }
            else{
                $item['type'] = "receiver";
            }
            
            $item['name'] = $sender['nom']." ".$sender['prenom'];
            
            $item['message'] = $message['messaqge'];
            
            array_push($All_Messages, $item);
            
        }
        
        echo $this->render(array(
            'messages' => $All_Messages,
                ), '../app/views/conversation.php');
        
    }
    
    /**
     * Send Message
     */
    public function SendMessage(){
        $messageManager = new \src\manager\MessageManager();
        $messageManager->AddMessage($_POST['message']);
    }

    /**
     * Render a page function
     * @param array $data
     * @param  string $url directory of view
     * @return type
     */
    public function render($data, $url) {
        extract($data);

        ob_start();
        include($url);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
    
}
