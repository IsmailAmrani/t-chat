<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace src\manager;

/**
 * Description of MessageManager
 *
 * @author eaismail
 */
class MessageManager {
    
    private $_db = null;
    
    public function __construct() {
         $this->_db = Database::getInstance();
    }
    
    /*
     *    Create Conversation betwen 2 users in not existe
     */
    public function CreateConversation($user1, $user2){
        
        $userManager = new UserManager();
        $user1_id = $userManager->getUser($user1)['id'];
        $user2_id = $userManager->getUser($user2)['id'];
        
        $query = $this->_db->prepare('SELECT * from conversation WHERE (user_1 = :user_1 AND user_2 = :user_2) OR (user_1 = :user_2 AND user_2 = :user_1)');
        $query->execute(array(
            ':user_1' => $user1_id,
            ':user_2' => $user2_id
        ));
        if($query->rowCount() == 0){
            $queryInsert = $this->_db->prepare('INSERT INTO conversation (user_1, user_2) VALUES (:user_1, :user_2)');
            $queryInsert->execute(array(
                ':user_1' => $user1_id,
                ':user_2' => $user2_id
            ));
        }
        return $query->fetch()['id'];
    }
    
    /*
     *    get All Messge By Conversation
     */
    public function getMessageByConversation($conversation){
        $query = $this->_db->prepare('SELECT * FROM `message` WHERE conversation_id = :conversation_id');
        $query->execute(array(
            ':conversation_id' => $conversation
        ));
        return $query->fetchAll();
    }
    
    /*
     *    Add Message
     */
    public function AddMessage($message){
        
        $userManager = new \src\manager\UserManager();
        
        $query = $this->_db->prepare('INSERT INTO `message` (conversation_id, sender, receiver, messaqge, date) VALUES (:conversation_id, :sender, :receiver, :messaqge, :date)');
        $query->execute(array(
            ':conversation_id' => $this->CreateConversation($_SESSION['user'], $_SESSION['conversation']),
            ':sender' => $userManager->getUser($_SESSION['user'])['id'],
            ':receiver' => $userManager->getUser($_SESSION['conversation'])['id'],
            ':messaqge' => $message,
            ':date' => date("Y-m-d H:i:s")
        ));
        
    }
    
}
