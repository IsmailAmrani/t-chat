<?php

namespace src\entity;

/**
 * Message
 */
class Message
{
    private $message;

    private $date;

    private $id;

    private $conversation;

    private $sender;

    private $receiver;


    /**
     * Set message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set conversation
     */
    public function setConversation($conversation = null)
    {
        $this->conversation = $conversation;

        return $this;
    }

    /**
     * Get conversation
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * Set sender
     */
    public function setSender($sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set receiver
     */
    public function setReceiver($receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     */
    public function getReceiver()
    {
        return $this->receiver;
    }
}

