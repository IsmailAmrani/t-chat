/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function ChangeMode(mode){
    switch (mode){
        case 'login':
            $('#Form_login').removeClass('d-none');
            $('#Form_register').addClass('d-none');
            break;
        case 'register':
            $('#Form_register').removeClass('d-none');
            $('#Form_login').addClass('d-none');
            break;
    }
}

var old_object = '';
function SelectConversation(email, object){
    $.ajax({
        data: { email: email },
        type: "POST",
        url: "conversation.php",
        success: function() {
            $('#'+object).addClass('Selected');
            if(old_object != ''){
                $('#'+old_object).removeClass('Selected');
            }
            old_object = object;
            $('#btn_send').removeAttr("disabled");
        }
    });
    getMessageConversation();
    var objDiv = document.getElementById("ListMessages");
    objDiv.scrollTop = objDiv.scrollHeight;
    StartListner();
}

function getMessageConversation(){
    $.ajax({
        type: "GET",
        url: "message_conversation.php",
        success: function(response) {
            $('#ListMessages').empty().append(response);
            
        }
    });
}

function SendMessage(){
    if($('#messageText').val() != ''){
        $.ajax({
            data: { message: $('#messageText').val() },
            type: "POST",
            url: "send_message.php",
            success: function(response) {
                $('#messageText').val('');
            }
        });
    }
}

function StartListner(){
    setInterval(function() {
        getMessageConversation();
        var objDiv = document.getElementById("ListMessages");
        objDiv.scrollTop = objDiv.scrollHeight;
    }, 1000);
}