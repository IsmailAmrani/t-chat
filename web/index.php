<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require '../src/autoloader.php';
src\autoloader::register();

session_start();

if(isset($_SESSION['user']) && !empty($_SESSION['user'])){
    if(basename($_SERVER['REQUEST_URI']) == 'message.php') {
        $Controller = new src\controller\MessageController();
        $Controller->Message();
    }
    else if(basename($_SERVER['REQUEST_URI']) == 'logout.php') {
        $Controller = new src\controller\UserController();
        $Controller->logout();
    }
    else if(basename($_SERVER['REQUEST_URI']) == 'conversation.php') {
        $Controller = new src\controller\MessageController();
        $Controller->CreateConversation();
    }
    else if(basename($_SERVER['REQUEST_URI']) == 'message_conversation.php') {
        $Controller = new src\controller\MessageController();
        $Controller->getMessagesByCurrentConversation();
    }
    else if(basename($_SERVER['REQUEST_URI']) == 'send_message.php') {
        $Controller = new src\controller\MessageController();
        $Controller->SendMessage();
    }
    else{
        $Controller = new src\controller\MessageController();
        $Controller->Message();
    }
}
else{
    $Controller = new src\controller\UserController();
    $Controller->login();
}