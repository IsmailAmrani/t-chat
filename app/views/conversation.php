<?php foreach ($messages as $message){
    if($message['type'] == 'sender'){ ?>
        <div class="m-3"><b><?php echo $message['name'] ?>:</b> <?php echo $message['message'] ?></div>
    <?php } else{ ?>
        <div class="m-3 text-right"><b><?php echo $message['name'] ?>:</b> <?php echo $message['message'] ?></div>
    <?php } ?>
<?php } ?>