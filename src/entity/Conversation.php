<?php

namespace src\entity;

/**
 * Conversation
 */
class Conversation
{
    private $id;

    private $user1;

    private $user2;


    /**
     * Get id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user1
     */
    public function setUser1($user1 = null)
    {
        $this->user1 = $user1;

        return $this;
    }

    /**
     * Get user1
     */
    public function getUser1()
    {
        return $this->user1;
    }

    /**
     * Set user2
     *
     * @return Conversation
     */
    public function setUser2($user2 = null)
    {
        $this->user2 = $user2;

        return $this;
    }

    /**
     * Get user2
     */
    public function getUser2()
    {
        return $this->user2;
    }
}

